package com.example.demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.junit.Assert;

import java.util.List;
import java.util.concurrent.TimeUnit;


//comment the above line and uncomment below line to use Chrome
//import org.openqa.selenium.chrome.ChromeDriver;
@Component
public class SelTest implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception{
        //System.setProperty("webdriver.gecko.driver", "C:\\Users\\d.larocca\\Desktop\\kubectl\\geckodriver.exe");
        System.setProperty("webdriver.gecko.driver","/var/geckodriver");
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("-headless");

        
        WebDriver driver = new FirefoxDriver(options);

        String baseUrl = "http://10.39.10.33:8086";

        driver.get(baseUrl);

        String page = null;
        
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        boolean isTheTextPresent = driver.getPageSource().contains("HELLO WORLD");

        Assert.assertTrue(isTheTextPresent);

        driver.quit();

    }
}
