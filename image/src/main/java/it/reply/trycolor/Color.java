package it.reply.trycolor;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.lang.Thread;
import java.io.*;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.Signature;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;

import java.util.Vector;

@RestController
public class Color {

    @GetMapping("/")
    public String getColor() {
        try{
            Signature sign = Signature.getInstance("SHA256withRSA");
        
            //Creating KeyPair generator object
            KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA");
            
            //Initializing the key pair generator
            keyPairGen.initialize(2048);
            
            //Generating the pair of keys
            KeyPair pair = keyPairGen.generateKeyPair();      
            
            //Creating a Cipher object
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                
            //Initializing a Cipher object
            cipher.init(Cipher.ENCRYPT_MODE, pair.getPublic());
            
            //Adding data to the cipher
            byte[] input = "HELLO WORLD! v022".getBytes();	  
            cipher.update(input);
            
            //encrypting the data
            byte[] cipherText = cipher.doFinal();
            
            Vector v = new Vector();
            for(int i=0;i<100;i++){
                byte b[] = new byte[1048576];
                v.add(b);
            }
            
            Thread.sleep(10);
            
        }catch(Exception e){}

        return "HELLO WORLD! v049";
    }
}
